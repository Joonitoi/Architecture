package com.gitlab.avelyn.architecture.utilites;


import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public interface Optional<Type> extends Supplier<Type> {
	
	static <Type> Optional<Type> from(final java.util.Optional<Type> value) {
		return of(value.orElse(null));
	}
	
	//TODO Do we want to call through, or call once and hold?
	static <Type> Optional<Type> from(final Supplier<Type> value) {
		return value::get;
	}
	
	@Deprecated
	static <Type> Optional<Type> ofNullable(Type value) {
		return of(value);
	}
	
	static <Type> Optional<Type> of(final Type value) {
		boolean present = value != null;
		return new Optional<Type>() {
			@Override
			public boolean isPresent() {
				return present;
			}
			
			
			@Override
			public Type get() {
				return value;
			}
		};
	}
	
	static <Type> Optional<Type> empty() {
		return new Optional<Type>() {
			@Override
			public boolean isPresent() {
				return false;
			}
			
			
			@Override
			public Type get() {
				return null;
			}
		};
	}
	
	
	default <Return> Optional<Return> map(Function<Type, Return> mapper) {
		return () -> {
			final Type value = get();
			return value != null ? mapper.apply(value) : null;
		};
	}
	
	default boolean isPresent() {
		return get() != null;
	}
	
	default Type orElseThrow(Supplier<Throwable> throwable) {
		return getOrThrow(throwable);
	}
	
	default Type orElseThrow(Throwable throwable) {
		return getOrThrow(throwable);
	}
	
	default Type getOrThrow(Supplier<Throwable> throwable) {
		return getOrThrow(throwable.get());
	}
	
	default Type getOrThrow(Throwable throwable) {
		Type value = get();
		if (value != null)
			return value;
		
		if (throwable instanceof RuntimeException)
			throw (RuntimeException) throwable;
		throw new RuntimeException(throwable);
	}
	
	
	default Optional<Type> or(Supplier<Type> alternative) {
		return new Optional<Type>() {
			@Override
			public Type get() {
				return alternative.get();
			}
			
			@Override
			public boolean isPresent() {
				return Optional.this.isPresent() || get() != null;
			}
		};
	}
	
	default Type orGet(Supplier<Type> alternative) {
		Type value = get();
		if (value != null)
			return value;
		return alternative.get();
	}
	
	default Type orElse(Type alternative) {
		return isPresent() ? get() : alternative;
	}
	
	
	@SuppressWarnings("unchecked")
	default <Cast> Optional<Cast> cast() {
		return (Optional<Cast>) this;
	}
	
	
	default Optional<Type> ifPresentOrElse(Consumer<Type> consumer, Runnable runnable) {
		return ifPresentOr(consumer, runnable);
	}
	
	
	default Optional<Type> ifPresentOr(Consumer<Type> consumer, Runnable runnable) {
		Type value = get();
		if (value != null)
			consumer.accept(value);
		else
			runnable.run();
		return this;
	}
	
	
	default Optional<Type> ifPresent(Consumer<Type> consumer) {
		Type value = get();
		if (value != null)
			consumer.accept(value);
		return this;
	}
	
	
	default Optional<Type> ifAbsent(Runnable runnable) {
		if (!isPresent())
			runnable.run();
		return this;
	}
	
	default java.util.Optional<Type> toJava() {
		return java.util.Optional.ofNullable(get());
	}
}
