package com.gitlab.avelyn.architecture.utilites;


public interface Enum<Type extends java.lang.Enum<Type>> {
	int flag();
	
	//--And--
	
	default Enum<Type> and(int values) {
		final int flag = flag() | values;
		return () -> flag;
	}
	
	
	default Enum<Type> and( Enum value) {
		return and(value.flag());
	}
	
	//--Is In--
	default boolean isIn( Enum value) {
		return isIn(value.flag());
	}
	
	default boolean isIn(int value) {
		return (flag() & value) != 0;
	}
	
	//--Not---
	
	default Enum<Type> not( Enum value) {
		return not(value.flag());
	}
	
	
	default Enum<Type> not(int values) {
		final int flag = flag() & ~values;
		return () -> flag;
	}
}
