package com.gitlab.avelyn.architecture.utilites;

public interface Clarifiers {
	@interface Millis {
	}
	
	@interface Text {
	}
	
	@interface Long {
	
	}
	
	@interface Integer {
	
	}
	
	@interface Double {
	
	}
}
